import re
from flask import Flask,render_template, request
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug.utils import  secure_filename, send_from_directory
from api.get_rgb import GetRgb, CompareColors, ConnectImage
from api.split import SplitOriginal
import shutil
import os
import pykakasi
import cv2
from PIL import Image
import glob





class Kakashi:
    kakashi = pykakasi.kakasi()
    kakashi.setMode("H", "a")
    kakashi.setMode("K", "a")
    kakashi.setMode("J", "a")
    conv = kakashi.getConverter()

    @classmethod
    def japanese_to_ascii(cls, japanese):
        return cls.conv.do(japanese)


app = Flask(__name__, static_folder="./build", static_url_path="/")


@app.route("/")
def serve():
    return send_from_directory(app.static_folder,'index.html')


@app.route("/download",methods=["GET", "POST"])
def download():
    if request.method == "GET":
        return render_template("index.html")
    elif request.method == "POST":
        data = request.files.getlist("file")
        for id,name in enumerate(data):
            ascii_filename = Kakashi.japanese_to_ascii(name.filename)
            save_filename = secure_filename(ascii_filename)
            name.save(os.path.join("./api/images/fullscale_images/download_material_files/", save_filename))

            img = cv2.imread(f"./api/images/fullscale_images/download_material_files/{save_filename}")
            height = img.shape[0]
            width = img.shape[1]
            adjust_height = (41 / height)
            adjust_width = (41/ width)
            img1 = cv2.resize(img, (int(width * adjust_width), int(height * adjust_height)))
            img2 = img1[0: 40, 0: 40]
            cv2.imwrite(f"./api/images/fullscale_images/big_material_files//canvas{id}.png", img2)
            img3 = cv2.resize(img2, (5, 5))
            cv2.imwrite(f"./api/images/fullscale_images/small_material_files//canvas{id}.png", img3)

        return {"bool": "true"}



@app.route("/upload",methods=["POST"])
def upload():
    if request.method == "POST":
        file = request.files["file"]
        ascii_filename = Kakashi.japanese_to_ascii(file.filename)
        save_filename = secure_filename(ascii_filename)
        file.save(os.path.join("./public/download_original_files/", save_filename))
        return {"image":save_filename}



@app.route("/create", methods=["GET", "POST"])
def create():
    name = glob.glob("./public/download_original_files/*")
    print(name)
    img = Image.open(name[0])
    img_resize = img.resize((400, 400))
    img_resize.save("./public/download_original_files/resize_original.png")


    if request.method == "GET":
        return render_template("index.html")
    elif request.method == "POST":
        size = int(request.json["num"])
        course = request.json["course"]
        filename = "./public/download_original_files/resize_original.png"


        if course == True:
            SplitOriginal(size, filename,"./api/images/split_original_files").split_image()
            read_original = GetRgb("/split_original_files")
            read_material = GetRgb("simple_images/small_material_files")
            cul = CompareColors(read_original.get_rgb(), read_material.get_rgb()).compare()
            create = ConnectImage(size, 400, cul,"simple_images")
            create.connect_image()

        elif course == False:
            SplitOriginal(size, filename,"./api/images/split_original_files").split_image()
            read_original = GetRgb("/split_original_files")
            read_material = GetRgb("fullscale_images/small_material_files")
            cul = CompareColors(read_original.get_rgb(), read_material.get_rgb()).compare()
            create = ConnectImage(size, 400, cul, "fullscale_images")
            create.connect_image()


        print("-"*50)
        return {"image":"mosaic_image.png"}

@app.route("/delete",methods=["GET","POST"])
def delete_files():
    course = request.json["del_course"]
    if course == True:
        shutil.rmtree("./public/download_original_files/")
        os.mkdir("./public/download_original_files/")
        shutil.rmtree("./public/upload_image_files/")
        os.mkdir("./public/upload_image_files/")
        shutil.rmtree("./api/images/split_original_files/")
        os.mkdir("./api/images/split_original_files/")
    elif course == False:
        shutil.rmtree("./api/images/fullscale_images/download_material_files/")
        os.mkdir("./api/images/fullscale_images/download_material_files/")
        shutil.rmtree("./api/images/fullscale_images/big_material_files/")
        os.mkdir("./api/images/fullscale_images/big_material_files/")
        shutil.rmtree("./api/images/fullscale_images/small_material_files/")
        os.mkdir("./api/images/fullscale_images/small_material_files/")
        shutil.rmtree("./public/download_original_files/")
        os.mkdir("./public/download_original_files/")
        shutil.rmtree("./public/upload_image_files/")
        os.mkdir("./public/upload_image_files/")
        shutil.rmtree("./api/images/split_original_files/")
        os.mkdir("./api/images/split_original_files/")


    return {"name":"showatanabe"}

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False, port=5000)
