import React, { useContext, memo } from "react";
import { saveAs } from "file-saver";
import { OpenDownloadContext } from "../providers/OpenDownloadProvider";
import GetAppIcon from "@material-ui/icons/GetApp";
import { IconButton } from "@material-ui/core";
import styled from "styled-components";

export const DownloadImage = memo(() => {
  console.log("hi!");

  const { openDownload } = useContext(OpenDownloadContext);
  const onClickDownload = () => {
    const url = `${process.env.PUBLIC_URL}/upload_image_files/mosaic_image.png`;
    saveAs(url, "mosaic_image");
  };

  const ButtonStyle = {
    color: "white",
    fontSize: "20px",
    fontWeight: "bold",
    borderRadius: "30px",
    width: "140px",
    height: "48px",
    radius: "30px",
  };

  return (
    <>
      {openDownload ? (
        <div>
          <SCustomIconButton style={ButtonStyle} onClick={onClickDownload}>
            <span>download</span>
            <GetAppIcon />
          </SCustomIconButton>
        </div>
      ) : null}
    </>
  );
});

const SCustomIconButton = styled(IconButton)`
  background: linear-gradient(45deg, #66b8cc, #65a7cc);
  &:hover {
    transform: translateY(-0.1rem);
    background: linear-gradient(45deg, #59a1b3, #cc6685);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;
