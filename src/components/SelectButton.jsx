import React from "react";
import { useHistory } from "react-router-dom";
import { IconButton } from "@material-ui/core";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import styled from "styled-components";

export const SelectButton = (props) => {
  console.log("hi!");

  const { openSelect, changeOpenSelect } = props;
  const history = useHistory();
  const onClickFinish = () => {
    history.push("/simple");
    changeOpenSelect();
  };

  const ButtonStyle = {
    color: "white",
    fontSize: "20px",
    fontWeight: "bold",
    borderRadius: "30px",
    width: "200px",
    height: "48px",
    radius: "30px",
  };

  return (
    <>
      {openSelect ? (
        <SCustomIconButton style={ButtonStyle} onClick={onClickFinish}>
          NEXT
          <span>
            <NavigateNextIcon />
          </span>
        </SCustomIconButton>
      ) : null}
    </>
  );
};

const SCustomIconButton = styled(IconButton)`
  background: linear-gradient(45deg, #cc7a93, #f5f29d);

  &:hover {
    transform: translateY(-0.1rem);
    background: linear-gradient(45deg, #f5f29d, #faf9d4);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;
