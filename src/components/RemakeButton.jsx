import React, { memo, useContext } from "react";
import { OpenDownloadContext } from "../providers/OpenDownloadProvider";
import logo from "../display.png";
import ReplayIcon from "@material-ui/icons/Replay";
import { IconButton } from "@material-ui/core";
import { OpenCreateContext } from "../providers/OpenCreateProvider";
import styled from "styled-components";
import axios from "axios";

export const RemaikButton = memo((props) => {
  console.log("hi!");

  const { changeImage } = props;
  const { setOpenCreate } = useContext(OpenCreateContext);
  const { setOpenDownload } = useContext(OpenDownloadContext);

  const ButtonStyle = {
    color: "white",
    fontSize: "20px",
    fontWeight: "bold",
    borderRadius: "30px",
    width: "120px",
    height: "48px",
    radius: "30px",
  };

  const onClickRemake = () => {
    const Clear = () => {
      axios.post("http://localhost:5000/delete", { del_course: true });
    };
    Clear();
    changeImage(logo);
    setOpenCreate(true);
    setOpenDownload(false);
  };

  return (
    <SStyledButton style={ButtonStyle} onClick={onClickRemake}>
      <span>remake</span>
      <ReplayIcon></ReplayIcon>
    </SStyledButton>
  );
});

const SStyledButton = styled(IconButton)`
  background: linear-gradient(45deg, #66b8cc, #65a7cc);
  &:hover {
    transform: translateY(-0.1rem);
    background: linear-gradient(45deg, #59a1b3, #ccad66);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;
