import React, { useState, useContext, memo } from "react";
import axios from "axios";
import { DownloadImage } from "./DownloadImage";
import { OpenDownloadContext } from "../providers/OpenDownloadProvider";
import { OpenCreateContext } from "../providers/OpenCreateProvider";
import { CourseContext } from "../providers/CourseProvider";
import { Button } from "@material-ui/core";
import styled from "styled-components";

// import ReactLoading from "react-loading";

export const CreateMosaicButton = memo((props) => {
  console.log("hi!");

  const { changeImage } = props;
  const { course } = useContext(CourseContext);
  const { openCreate } = useContext(OpenCreateContext);
  const { openDownload, setOpenDownload } = useContext(OpenDownloadContext);
  const [imageSize, setImageSize] = useState();
  const onChangeSelectNum = (e) => setImageSize(e.target.value);

  const ButtonStyle = {
    color: "black",
    fontSize: "15px",
    fontWeight: "bold",
    borderRadius: "30px",
    width: "60px",
    height: "48px",
    radius: "30px",
    border: "2px solid skyblue",
  };

  const onClickCreateImage = (e) => {
    e.preventDefault();
    const submitImage = () => {
      axios
        .post(
          "http://localhost:5000/create",
          { num: imageSize, course: course },
          {
            header: { "Content-type": "application/json; charset=utf-8" },
          }
        )
        .then((result) => {
          setOpenDownload(!openDownload);
          changeImage(
            `${process.env.PUBLIC_URL}/upload_image_files/${result.data.image}`
          );
        });
    };
    submitImage();
  };

  return (
    <>
      {openCreate === openDownload && (
        <div>
          <form onSubmit={onClickCreateImage} encType="multipart/form-data">
            <Select id="dropdown" onChange={onChangeSelectNum}>
              <option value="20">size</option>
              <option value="5">5</option>
              <option value="8">8</option>
              <option value="10">10</option>
              <option value="20">20</option>
            </Select>
            <SStyledButton style={ButtonStyle} type="submit">
              作成
            </SStyledButton>
          </form>
        </div>
      )}

      <div>
        <DownloadImage />
      </div>
    </>
  );
});

const Select = styled.select`
  overflow: hidden;
  width: 80px;
  height: 48px;
  font-size: 20px;
  font-weight: bold;
  text-align: center;
  border-radius: 2px;
  border: 2px solid skyblue;
  border-radius: 50px;
  background: #ffffff;
`;

const SStyledButton = styled(Button)`
  background: linear-gradient(45deg, #fff, #fff);

  &:hover {
    transform: translateY(-0.1 rem);
    background: linear-gradient(45deg, #ffffff, #accfcc);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;
