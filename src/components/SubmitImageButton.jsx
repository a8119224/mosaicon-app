import React, { useContext } from "react";
import axios from "axios";
import { CreateMosaicButton } from "./CreateMosaicButton";
import { RemaikButton } from "./RemakeButton";
import { OpenCreateContext } from "../providers/OpenCreateProvider";
import { Button, IconButton } from "@material-ui/core";
import PublishIcon from "@material-ui/icons/Publish";
import styled from "styled-components";

export const SubmitImageButton = (props) => {
  console.log("hi!");

  const { changeImage } = props;
  const { openCreate, setOpenCreate } = useContext(OpenCreateContext);

  const ButtonStyle = {
    color: "white",
    fontSize: "20px",
    fontWeight: "bold",
    borderRadius: "30px",
    width: "120px",
    height: "48px",
    radius: "30px",
  };

  const submitImage = (e) => {
    e.preventDefault();
    setOpenCreate(!openCreate);
    const formData = new FormData(e.target);
    const Upload = () => {
      axios
        .post("/upload", formData, {
          header: { "content-type": "multipart/form-data" },
        })
        .then((result) => {
          changeImage(
            `${process.env.PUBLIC_URL}/download_original_files/${result.data.image}`
          );
        });
    };
    Upload();
  };

  return (
    <>
      {openCreate ? (
        <div>
          <Form onSubmit={submitImage} encType="multipart/form-data">
            <label>
              <input
                style={{ display: "none" }}
                type="file"
                name="file"
                accept="image/*"
              />
              <SStyledButton style={ButtonStyle} component="span">
                SELECT
              </SStyledButton>
            </label>
            <SCustomIconButton style={ButtonStyle} type="submit">
              <span>upload</span>
              <PublishIcon />
            </SCustomIconButton>
          </Form>
        </div>
      ) : (
        <Div>
          <CreateMosaicButton changeImage={changeImage} />
          <RemaikButton changeImage={changeImage} />
        </Div>
      )}
    </>
  );
};

const Form = styled.form`
  display: flex;
  justify-content: center;
  gap: 0 40px;
`;

const Div = styled.div`
  display: flex;
  justify-content: center;
  gap: 0 40px;
`;

const SStyledButton = styled(Button)`
  background: linear-gradient(45deg, #66b8cc, #65a7cc);

  &:hover {
    transform: translateY(-0.1rem);
    background: linear-gradient(45deg, #59a1b3, #4d8a99);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;

const SCustomIconButton = styled(IconButton)`
  background: linear-gradient(45deg, #66b8cc, #65a7cc);
  &:hover {
    transform: translateY(-0.1rem);
    background: linear-gradient(45deg, #59a1b3, #4d8a99);
    boxshadow: "0 3px 5px 2px rgba(255, 105, 135, 0.3)";
  }
  &:active {
    transform: translateY(-0.1rem);
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
  }
`;
