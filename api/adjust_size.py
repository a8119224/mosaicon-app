import cv2
import os

class AdjustSize:

  def __init__(self, filename, read_filepath, save_filepath,savename):
    self.filename = filename
    self.read_filepath = read_filepath
    self.save_filepath = save_filepath
    self.savename = savename

  def resize_image(self):
    img = cv2.imread(f"{self.read_filepath}{self.filename}")
    height = img.shape[0]
    width = img.shape[1]
    adjust_height = (403 / height)
    adjust_width = (403/ width)
    img1 = cv2.resize(img, (int(width * adjust_width), int(height * adjust_height)))
    img2 = img1[0: 400, 0: 400]
    cv2.imwrite(f"{self.save_filepath}{self.savename}",img2)
